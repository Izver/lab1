package com.example.lab1;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void firstMore_max_isCorrect() {
        assertEquals(MainActivity.my_max(20, 19), 20);
    }

    @Test
    public void same_max_isCorrect() {
        assertEquals(MainActivity.my_max(20, 20), 20);
    }

    @Test
    public void secondMore_max_isCorrect() {
        assertEquals(MainActivity.my_max(100, 200), 200);
    }

    @Test
    public void firstNegative_max_isCorrect() {
        assertEquals(MainActivity.my_max(-20, 19), 19);
    }

    @Test
    public void secondNegative_max_isCorrect() {
        assertEquals(MainActivity.my_max(18, -19), 18);
    }

    @Test
    public void bothNegative_max_isCorrect() {
        assertEquals(MainActivity.my_max(-18, -19), -18);
    }

    @Test
    public void bothNegativeSame_max_isCorrect() {
        assertEquals(MainActivity.my_max(-18, -18), -18);
    }
    //////////
    @Test
    public void firstMore_min_isCorrect() {
        assertEquals(MainActivity.my_min(20, 19), 19);
    }

    @Test
    public void same_min_isCorrect() {
        assertEquals(MainActivity.my_min(20, 20), 20);
    }

    @Test
    public void secondMore_min_isCorrect() {
        assertEquals(MainActivity.my_min(100, 200), 100);
    }

    @Test
    public void firstNegative_min_isCorrect() {
        assertEquals(MainActivity.my_min(-20, 19), -20);
    }

    @Test
    public void secondNegative_min_isCorrect() {
        assertEquals(MainActivity.my_min(18, -19), -19);
    }

    @Test
    public void bothNegative_min_isCorrect() {
        assertEquals(MainActivity.my_min(-18, -19), -19);
    }

    @Test
    public void bothNegativeSame_min_isCorrect() {
        assertEquals(MainActivity.my_min(-18, -18), -18);
    }

}